/**
 * Created by cristian on 1/25/16.
 */
var directives = angular.module('directives',[]);

directives.directive('ispinner', function() {
    return {
        restrict: 'AE',
        scope: {},
        templateUrl: 'views/directives/ispinner.html'
    };
});