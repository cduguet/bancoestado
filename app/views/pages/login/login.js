/**
 * Created by cristian on 1/15/16.
 */
var loginPageModule = angular.module('loginPageModule',[]);

// Used inline annotation to prevent breaking functions which use "$scope" injection
loginPageModule.controller('LoginPageController', [function() {
}]);