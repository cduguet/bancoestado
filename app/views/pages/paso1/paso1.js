/**
 * Created by cristian on 1/15/16.
 */
var paso1PageModule = angular.module('paso1PageModule',['factories', 'rzModule'])

// Used inline annotation to prevent breaking functions which use "$scope" injection
paso1PageModule.controller('PasoUnoPageController', ['$scope', '$rootScope', 'API', '$location', '$timeout', function($scope, $rootScope, API, $location, $timeout) {

    $rootScope.money = function(value) {
        return '$' + API.chileanFormat(value,0);
    };
    $rootScope.percent = function(value) {
        return API.chileanFormat(value,2) + '%';
    };
    $rootScope.eachMonth = function(e) {
        return moment(e).format("D")+ " de cada mes";
    };
    $rootScope.chileanDate = function(e) {
        return moment(e).format('L');
    };
    $rootScope.ufvalue = function(value) {
        return API.chileanFormat(value,2);
    };


    // JQuery DateTimePicker
    var begin = new Date();
    begin.setDate(begin.getDate() + 31);
    var end = new Date();
    end.setDate(end.getDate() + 90);
    $scope.paymentDay = begin;
    $('#datetimepicker1').datetimepicker({
        format: 'DD/MM/YYYY',
        locale: 'es',
        minDate: begin,
        maxDate: end,
        ignoreReadonly: true,
        focusOnShow: false
    });
    $("#datetimepicker1").on("dp.change", function(e) {
        var primerPago = e.date;
        var element = angular.element($('#datetimepicker1'));
        var scope = element.scope();
        scope.$apply(function() {
           scope.paymentDay = primerPago;
        });
        $("#paymentDayAlert").collapse("show");
        fireSpinners();
    });


    //Sliders
    $scope.sliderAmount = {
        value: 3000000,
        options: {
            floor: 500000,
            ceil: 3000000,
            step: 50000,
            getSelectionBarColor: function() {
                return '#f0f0ee';
            },
            translate: function(value) {
                return $scope.money(value);
            }, onChange: function() {
                fireSpinners();
                updateValues();
            }
        }
    };

    $scope.sliderMonths = {
        value: 12,
        options: {
            floor: 12,
            ceil: 60,
            step: 1,
            getSelectionBarColor: function(value) {
                return '#f0f0ee';
            },
            translate: function(value) {
                return value + ' meses';
            }, onChange: function() {
                fireSpinners();
                updateValues();
            }
        }
    };


    $("#account").change(function() {
        $("#accountAlert").collapse("show");
    });

    // Function to calculate values !!!
    $scope.interes = 1.48;
    $scope.impuesto = 20873;
    $scope.notario  = 1200;

    var seguroDesgravamen = 41473;
    var seguroProtegido = 183872;
    var UF = 25629.9;

    $.getJSON('http://mindicador.cl/api', function(data) {
        UF = data.uf.valor;
    }).fail(function() {
        console.log('Error al consumir la API de UF!');
    });


    $("input[name='insurance']:radio").change(function () {
        var selected = $("input[type='radio'][name='insurance']:checked");
        $scope.$apply(function() {
            $scope.selectedInsurance = selected.val();
        });
    });

    $scope.selectedInsurance = 1;

    var updateInsurance = function() {
        console.log($scope.selectedInsurance);
        if ($scope.selectedInsurance == 0) {
            $scope.seguro = seguroProtegido;
            console.log("Seguro Protegido chosen ");
        } else if ($scope.selectedInsurance == 1) {
            $scope.seguro = seguroDesgravamen;
            console.log("Seguro Desgravamen chosen ");
        } else if ($scope.selectedInsurance == 2) {
            $scope.seguro = 0;
            console.log("Sin Seguro chosen ");
        } else {
            $scope.seguro = 0;
            console.log("something else chosen ");
        }
    };

    updateInsurance();
    updateValues();

    $scope.$watchGroup(['sliderAmount', 'sliderMonths', 'selectedInsurance'], function(newValues, oldValues, scope) {
        console.log('updating values');
        updateInsurance();
        fireSpinners();
        updateValues();
    })

    function updateValues() {
        $scope.seguroUF = $scope.seguro / UF;
        $scope.montoTotal = $scope.sliderAmount.value + $scope.impuesto + $scope.notario + $scope.seguro;
        $scope.costoTotal = $scope.montoTotal / $scope.sliderMonths.value
            * (1 - Math.pow(1 + $scope.interes / 100 ,$scope.sliderMonths.value) ) / ( 1 - ( 1 + $scope.interes / 100  ) );
        $scope.valorCuota = $scope.costoTotal / $scope.sliderMonths.value ;
        $scope.cae = ( $scope.costoTotal / $scope.sliderAmount.value - 1) * 100;
    }



    $scope.accounts = ["Cta Cte 0000012345", "Cta Cte 0000054321"] ;

    $scope.getAccount = function() {
        try {
            return accounts[$scope.accounts[$scope.account]];
        } catch (err) {
            return '';
        }
    }

    function fireSpinners() {
        $("ispinner").show();
        setTimeout(function(){
            $("ispinner").hide();
        }, 1000);
    }

    $('#goTop').click(function() {
        var pos = $('#top').offset().top;
        $('html, body').animate({'scrollTop' : pos}, 300);
        return false;
    });


    $("#account").on("change", function() {
        fireSpinners();
        $("#account-alert").collapse('hide');
    });


    $scope.submit = function() {
        if ($scope.account == undefined || ($scope.account !=0 && $scope.account != 1))  {
            $("#account-alert").collapse('show');
            return;
        } else {
            $rootScope.valorCuota = $scope.valorCuota;
            $rootScope.costoTotal = $scope.costoTotal;
            $rootScope.montoSolicitado = $scope.sliderAmount.value;
            $rootScope.montoTotal = $scope.montoTotal;
            $rootScope.months = $scope.sliderMonths.value;
            $rootScope.interes = $scope.interes;
            $rootScope.impuesto = $scope.impuesto;
            $rootScope.notario = $scope.notario;
            $rootScope.seguro = $scope.seguro;
            $rootScope.seguroUF = $scope.seguroUF;
            $rootScope.paymentDay = $scope.paymentDay;
            $rootScope.cae = $scope.cae;
            $rootScope.account = $scope.accounts[$scope.account];
            $('#pageLoadModal').modal('show');
            $timeout(function(){
                $("#pageLoadModal").modal('hide');
                $location.path('/paso2');
            }, 1000);
        }
    };

    $(".close").click(function() {
        $(this).parent().collapse("hide");
    });

    $("#defaultInsurance").trigger('click');



    //    ******************************************* Close Model with Back Button  *****************************************************
    $(".modal:not(#pageLoadModal)").on("shown.bs.modal", function()  { // any time a modal is shown
        var urlReplace = "#" + $(this).attr('id'); // make the hash the id of the modal shown
        history.pushState(null, null, urlReplace); // push state that hash into the url
    });

    // If a pushstate has previously happened and the back button is clicked, hide any modals.
    $(window).on('popstate', function() {
        $(".modal").modal('hide');
    });




}]);