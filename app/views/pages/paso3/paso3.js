/**
 * Created by cristian on 1/15/16.
 */
var paso3PageModule = angular.module('paso3PageModule',[]);

// Used inline annotation to prevent breaking functions which use "$scope" injection
paso3PageModule.controller('Paso3PageController', [function() {
    //app logic
    $("#fecha-solicitud").html(moment(new Date()).format("L"));

    $('#comprobante-credito').addClass('x3');
    html2canvas([document.getElementById('comprobante-credito')], {
        onrendered: function (canvas) {

            $('#comprobante-credito').hide();
            var data = canvas.toDataURL('image/png');
            // AJAX call to send `data` to a PHP file that creates an image from the dataURI string and saves it to a directory on the server

            var image = new Image();
            image.src = data;
            document.getElementById('comprobante-imagen').appendChild(image);
        }
    });
}]);