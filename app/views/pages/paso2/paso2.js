/**
 * Created by cristian on 1/15/16.
 */
var paso2PageModule = angular.module('paso2PageModule',[]);

// Used inline annotation to prevent breaking functions which use "$scope" injection
paso2PageModule.controller('Paso2PageController', ['$rootScope', '$scope', '$location', '$timeout', function($rootScope, $scope, $location, $timeout) {

    $("#download").click(function () {
        console.log("clocking on");

        //Set time and date
        $("#fecha-y-hora").html(moment(new Date()).format("DD-MM-YYYY HH:mm"));
        $("#fecha-solicitud").html(moment(new Date()).format("L"));

        //hide big labels and show printable info
        $(".non-printable").hide();
        $(".printable").show();


        $('#detalles-printable').addClass('x2');

        html2canvas([document.getElementById('detalles-printable')], {
            onrendered: function (canvas) {
                $('#detalles-printable').removeClass('x2');
                // Show big labels and hide printables
                $(".printable").hide();
                $(".non-printable").show();


                //document.getElementById('canvas').appendChild(canvas);
                var data = canvas.toDataURL('image/png');
                // AJAX call to send `data` to a PHP file that creates an image from the dataURI string and saves it to a directory on the server

                var image = new Image();
                image.src = data;
                // Get the image Div
                var imageDiv = document.getElementById("download-body");

                // If the Div element has any child nodes, remove its first child node
                if (imageDiv.hasChildNodes()) {
                    imageDiv.removeChild(imageDiv.childNodes[0]);
                }
                imageDiv.appendChild(image);
            }, background: "#fff"
        });

    });

    $(document).ready(function () {
        $('#code1').keyup(function () {
            if (this.value.length == $(this).attr("maxlength")) {
                $('#code2').focus();
            }
        });
        $('#code2').keyup(function () {
            if (this.value.length == $(this).attr("maxlength")) {
                $('#code3').focus();
            }
        });
        $('#code3').keyup(function () {
            if (this.value.length == $(this).attr("maxlength")) {
                $('#code3').blur();
                $('#nextButton').focus();
            }
        });

    });

//    ******************************************* Alert Functions *****************************************************
    $(function () {
        $('[data-toggle="popover"]').popover()
    });

    $('#accept-checkbox').change(function() {
        if (this.checked) {
            $("#accept-alert").collapse("hide");
        }
    });

    $scope.next = function() {

        // if (!checkToken()) {
        //   console.log("showing token alert");
        //   $("#token-alert").collapse("show");

        // }
        if (!$("#accept-checkbox").is(":checked")) {
            console.log("showing alert");
            $("#accept-alert").collapse("show");
        }

        if ($("#accept-checkbox").is(":checked")) {
            $('#pageLoadModal').modal('show');
            $timeout(function(){
                $("#pageLoadModal").modal('hide');
                $location.path('/paso3');
            }, 1000);
        }

    };

    $(".close").click(function() {
        $(this).parent().collapse("hide");
    });

//    ******************************************* Close Model with Back Button  *****************************************************
    $(".modal:not(#pageLoadModal)").on("shown.bs.modal", function()  { // any time a modal is shown
        var urlReplace = "#" + $(this).attr('id'); // make the hash the id of the modal shown
        history.pushState(null, null, urlReplace); // push state that hash into the url
    });

    // If a pushstate has previously happened and the back button is clicked, hide any modals.
    $(window).on('popstate', function() {
        $(".modal").modal('hide');
    });

}]);