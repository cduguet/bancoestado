/**
 * Created by cristian on 1/15/16.
 */
var homePageModule = angular.module('homePageModule',[]);

// Used inline annotation to prevent breaking functions which use "$scope" injection
homePageModule.controller('HomePageController', [function() {
    //app logic
}]);