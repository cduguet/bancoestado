'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myApp', ['ngRoute',
    'loginPageModule',
    'homePageModule',
    'paso1PageModule',
    'paso2PageModule',
    'paso3PageModule',
    'myApp.version',
    'directives'
]);
app.constant('_',_);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.
    when('/', {
        templateUrl: 'views/pages/login/login.html',
        controller: 'LoginPageController'
    }).when('/home', {
        templateUrl: 'views/pages/home/home.html',
        controller: 'HomePageController'
    }).when('/paso1', {
        templateUrl: 'views/pages/paso1/paso1.html',
        controller: 'PasoUnoPageController'
    }).when('/paso2', {
        templateUrl: 'views/pages/paso2/paso2.html',
        controller: 'Paso2PageController'
    }).when('/paso3', {
        templateUrl: 'views/pages/paso3/paso3.html',
        controller: 'Paso3PageController'
    })
}]);



app.controller('NavigationController', ['$scope', '$location', '_',
    function ($scope, $location, _) {
        //create menu items in the nav bar dinamically

        $scope.menus = [{
            name: 'Mi Perfil',
            url: '#',
            class: ''
        }, {
            name: 'Cuenta Corriente',
            url: '#',
            class: ''
        }, {
            name: 'Cuenta de Ahorro',
            url: '#',
            class: ''
        }, {
            name: 'Cuenta RUT',
            url: '#',
            class: ''
        }, {
            name: 'Simular Crédito',
            url: '#',
            class: ''
        }, {
            name: 'Pagar Cuentas',
            url: '#',
            class: ''
        }, {
            name: 'Recargas',
            url: '#',
            class: ''
        }];

        var updateNavigation = function () {
            var url = $location.url();
            _.each($scope.menus, function (menu) {
                if (menu.url === url || (menu.url + '/') === url) {
                    menu.class = 'active';
                } else {
                    menu.class = '';
                }
            });
        };

        $scope.$on('$routeChangeSuccess', function () {
            updateNavigation();
        });

        updateNavigation();
    }]);